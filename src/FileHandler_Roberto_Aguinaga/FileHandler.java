/*
 * This class provides methods to create and operate on files. It
 * can create, write to, read from, and delete files. It can also
 * handle (most) exceptions that may be thrown during IO operations.
 */
package FileHandler_Roberto_Aguinaga;

import java.io.IOException;

import java.io.*;

import java.util.Scanner;

public class FileHandler {
    public static String createFile(String fileName) {
        File file = new File(fileName);

        try {
            if (file.createNewFile())
                return fileName + " successfully created.";
            else
                return "Something went wrong while creating the file.";
        } catch (IOException e) {
            return e.getMessage();
        }
    }

    /**
     * Attempts to open a file, returns an error message if an
     * exception is thrown.
     *
     * @param fileName A string for a file name
     * @return String array with content to display and error messages.
     */
    public static String[] readFile(String fileName) {
        // All text to be displayed is in String 0
        // Error messages are contained in String 1
        // ... Forgot that strings are "initialized" with null >:(
        // Thus, two empty strings for the array, else concat in the
        // while loop throws a null pointer exception.

        String[] stringArray = {"", ""};
        File file = new File(fileName);

        try (Scanner fileScanner = new Scanner(file)) {
            while (fileScanner.hasNextLine()) {
                stringArray[0] =
                        stringArray[0].concat(fileScanner.nextLine() + "\n");
            } // End of while loop
            stringArray[1] = String.format("Successfully read " +
                    "from %s.", fileName);
            return stringArray;
        } catch (FileNotFoundException e) {
            stringArray[0] = "";
            stringArray[1] = "File not found.";
            return stringArray;
        }
    }

    /**
     * Writes to a file that already exists. If the file is not
     * found, shows an error message.
     *
     * @param fileName Name of the file to write to
     * @param contents Stuff to write to the file.
     * @return Message about whether the operation was successful or
     * not.
     */
    public static String writeFile(String fileName, String contents) {
        try {
            File fileToWrite = new File(fileName);
            if (!fileToWrite.exists()) {
                throw new FileNotFoundException();
            } else {
                /* Inner try-with-resources block creates two
                resources: a FileWriter and new PrintWriter. The
                FileWriter object is necessary to append data to an
                existing file, rather than creating a new one. */
                try (FileWriter fWriter = new FileWriter(fileName,
                        true);
                     PrintWriter fileWriter = new PrintWriter(fWriter)) {
                    fileWriter.println(contents);
                } catch (IOException ioException) {
                    return "Could not write to file";
                }
            }
            return "Successfully wrote to " + fileName;
        } catch (FileNotFoundException e) {
            return "File not found";
        }
    }

    /**
     * Deletes a file, returns a message about the result of the
     * operation.
     *
     * @param fileName The file to delete
     * @return A message about the result of the operation.
     */
    public static String deleteFile(String fileName) {
        // Create file object
        File fileToDelete = new File(fileName);
        if (fileToDelete.exists() && fileToDelete.delete()) {
            return fileName + " was successfully deleted.";
        } else {
            return fileName + " was not deleted.";
        }
    }

}